#ifndef USER_H
#define USER_H

#include <stdio.h>

struct User {
    char name[100];
    unsigned char age;
    char address[255];
    char cnp[14];
};

void printUser(struct User user){
    printf("Name: %s\nAge: %d\nAddress: %s\nCNP: %s\n",
            user.name, user.age, user.address, user.cnp);
}

#endif

