/*
 * Autor: Radu Hobincu
 * Cerinta: Switch coding style
 * Data: 18.11.2015
 * Copyright io!
 */
 
#include <stdio.h>

int main() {
    char letter;
    printf("Introduceti o litera: ");
    scanf("%c", &letter);
    
    switch (letter) {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            printf("Caracterul este o vocala mica.\n");
            break;
        case 'A':
        case 'E':
        case 'I':
        case 'O':
        case 'U':
            printf("Caracterul este o vocala mare.\n");
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            printf("Caracterul este o cifra.\n");
            break;
        default: printf("Caracterul este necunoscut.\n");
    }
    
    return 0;
}
