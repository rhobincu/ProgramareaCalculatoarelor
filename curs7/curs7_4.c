/*
 * Autor: Radu Hobincu
 * Cerinta: ifdef
 * Data: 18.11.2015
 * Copyright io!
 */

#include <stdio.h>

 
int main() {

#if DEBUG == 1
    printf("Debug is enabled\n");
#endif

    if (DEBUG) {
        printf("Debug is enabled: %d\n", 0x123);
    }

    return 0;
}
 
