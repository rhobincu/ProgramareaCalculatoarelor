/*
 * Autor: Radu Hobincu
 * Cerinta: if-else-if-else coding style
 * Data: 18.11.2015
 * Copyright io!
 */
 
#include <stdio.h>

int main() {
    char letter;
    printf("Introduceti o litera: ");
    scanf("%c", &letter);
    
    if (letter > 31 && letter < 60) {
        //
    } else if (letter > 61 && letter < 90) {
        //
    } else if (letter > 91 && letter < 100) {
        //
    } else {
        // kasdl
    }
}
