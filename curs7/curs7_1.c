/*
 * Autor: Radu Hobincu
 * Cerinta: Să se citească un număr întreg fără semn n de la tastatură. Să se afișeze primele n numere prime.
 * Data: 18.11.2015
 * Copyright io!
 */
 
#include <stdio.h>

int main() {
    unsigned value;
    printf("Introduceti valoarea: ");
    scanf("%u", &value);
    
    unsigned primeCounter = 0;
    unsigned currentNumber = 2;
    while (primeCounter != value) {
        unsigned dividers = 0;
        unsigned j;
        for(j = 2; j < currentNumber - 1; j++) {
            if (currentNumber % j == 0) {
                dividers++;
                break;
            }
        }
        if (!dividers) {
            printf("%u ", currentNumber);
            primeCounter++;
        }
        currentNumber++;
    }
    printf("\n");
    return 0;
}

