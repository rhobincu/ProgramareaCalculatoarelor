#include <stdio.h>

int main(){
    char string[100];
    short array[33];
    
    printf("Dimensiunea in bytes a unui int este: %ld\n", sizeof(int));
    printf("Dimensiunea in bytes a unui long este: %ld\n", sizeof(long));
    printf("Dimensiunea in bytes a unui long long este: %ld\n", sizeof(long long));
    printf("Dimensiunea in bytes a unui char este: %ld\n", sizeof(char));
    printf("Dimensiunea in bytes a variabilei string este: %ld\n", sizeof(string));
    printf("Dimensiunea in bytes a variabilei array este: %ld\n", sizeof(array));
    printf("Dimensiunea in bytes a unui long[30] este: %ld\n", sizeof(long[30]));
    return 0;
}
